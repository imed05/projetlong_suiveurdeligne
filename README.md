


Robot mindstorm

Ce projet permet à un robot/automate, qui peut se déplacer, de suivre un chemin préalablement tracé au sol (par des humains ou par d’autres robots). 
Il donne alors une certaine  autonomie de mobilité au robot, et la possibilité d’encadrer, par un circuit, son trajet.