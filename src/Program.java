import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.motor.Motor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;

//Programme de notre robot : permet de le faire suivre des lignes de coueurs
public class Program {
	ColorSensor color_sensor; //permet de detecter les couleurs
	SampleProvider sample;
	float[] color; //tableau de nos couleurs detectees
	int id_color; //indice de la couleur principale
	int id_stop; //indice de la couleur d'arret
	int actual_nb_tour; //nombre de tour actuel
	final int wanted_nb_tour = 2; //nombre de tour souhaites
	DifferentialPilot pilot;

	public Program() { 
		//Initialisation
		actual_nb_tour = 0;
		pilot = new DifferentialPilot(1.5f, 6, Motor.A, Motor.D);
		color_sensor = new ColorSensor(3);
		color_sensor.calibrer();

		sample = color_sensor.ev3.getRGBMode();
		color = new float[sample.sampleSize()];
		//detection des couleurs
		detectMainColor();
		detectBlackColor();
	}

	public void run() { //fonction qui permet de suivre une ligne de couleur
		int direction = 1;
		int speed1 = 100; //vitesses de nos moteurs apres test de plusieurs vitesses differentes
		int speed2 = (int) Motor.A.getMaxSpeed()-100;
		boolean saw_black = false;

		while (Button.ESCAPE.isUp()) { //permet de quitter le programme a tout moment

			while ((id_color == color_sensor.detectionColor() || id_stop == color_sensor.detectionColor()) 
					&& Button.ESCAPE.isUp()) {
				Motor.A.forward(); //avancer
				Motor.D.forward();
				int i = color_sensor.detectionColor();
				if(id_color == i){ //si c'est la couleur principale
					if(saw_black){
						actual_nb_tour ++; //on a fait un tour
						if(actual_nb_tour == wanted_nb_tour){ //si on a fait le nombre de tours voulu
							Motor.A.stop(); //alors on s'arrete
							Motor.D.stop();
							return;	//et on arrete le programme
						}
						saw_black = false;
					}
					if (direction == 1) { //si on a detecter la couleur principale on continue d'avancer
						Motor.A.setSpeed(speed2); //une roue va plus vite que l'autre pour pouvoir tourner sans perdre de temps
						Motor.D.setSpeed(speed1);
					} else {
						Motor.D.setSpeed(speed2);
						Motor.A.setSpeed(speed1);
					}
				}else if (id_stop == i){ //si on a detecter la couleur d'arret
					Sound.beep();
					saw_black = true;
					Delay.msDelay(50);	
				}
			}
			
			while ((id_color != color_sensor.detectionColor()) && Button.ESCAPE.isUp()) { //quand on a perdu la ligne
				Motor.A.forward();
				Motor.D.forward();
				//on retourne sur la ligne en fonction de la derniere direction prise avant d'avoir perdu la ligne
				if (direction != 1) { 
					Motor.A.setSpeed((int) Motor.A.getMaxSpeed());
					Motor.D.setSpeed(speed1);
				} else {
					Motor.A.setSpeed(speed1);
					Motor.D.setSpeed((int) Motor.D.getMaxSpeed());
				}
			}
			//maintenant la direction change car on a retrouver la ligne
			direction *= -1;
		}
	}

	public void detectMainColor() { //permet de detecter quelle couleur est la principale donc celle a suivre
		System.out.println("Presentez la couleur principale");
		while(Button.ENTER.isUp())
			Delay.msDelay(100);
		id_color = color_sensor.detectionColor();
		Sound.twoBeeps();
	}
	
	public void detectBlackColor() { //permet de detecter la couleur d'arret 
		System.out.println("Presentez la couleur d'arret");
		while(Button.ENTER.isUp())
			Delay.msDelay(100);
		id_stop = color_sensor.detectionColor();
		Sound.twoBeeps();
	}
	
	public static void main(String[] args) {
		Program p = new Program();
		Delay.msDelay(2000);
		p.run();
	}
}
