import lejos.hardware.Button;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

//Permet de detecter les couleurs pour notre robot EV3
public class ColorSensor {

	EV3ColorSensor ev3; //le detecteur de couleur
	SampleProvider sample;
	float[] r_color; //les composantes rouges des couleurs detectees
	float[] g_color; //les composantes vertes des couleurs detectees
	float[] b_color; //les composantes bleus des couleurs detectees
	int nb_color; //nombre de couleurs
	
	public ColorSensor(int n) {
		//Initialisation
		nb_color = n;
		r_color = new float[nb_color];
		g_color = new float[nb_color];
		b_color = new float[nb_color];
		ev3 = new EV3ColorSensor(SensorPort.S1); //detecteur de couleur ev3
		sample = ev3.getRGBMode(); 
	}
	
	public void calibrer () { //permet de detecter et retenir les couleurs presentes pour la suite du programme
		int n = 20;
		float [] color = new float[n];
		for(int j = 0; j<nb_color; j++) {
			r_color[j] = 0;
			g_color[j] = 0;
			b_color[j] = 0;
			//demande de presenter une nouvelle couleur a enregistrer
			System.out.println("Nouvelle couleur a calibrer");
			while(Button.ENTER.isUp()) //permet de quitter a tout moments
				Delay.msDelay(100);
			for(int i = 0; i < n; i++) { //un certain nombre n de captures de la couleur pour plus de precision
				sample.fetchSample(color, 0);
				r_color[j] += color[0];
				g_color[j] += color[1];
				b_color[j] += color[2];				
			}
			//calcul de la valeur moyenne de chacunes des composantes (RGB) de la couleur
			r_color[j] /= n;
			g_color[j] /= n;
			b_color[j] /= n;
			Delay.msDelay(100);
		}		
	}
	
	public int detectionColor() { //retourne l'indice de la couleur detectee par le robot
		
		float [][] diff = new float[nb_color][3];
		float[] color = new float[3];
		sample.fetchSample(color, 0);
		//calcul des differences de valeurs des composantes avec les couleurs connues
		for(int i = 0; i < nb_color; i++) {
			diff[i][0] = Math.abs(color[0] - r_color[i]);	
			diff[i][1] = Math.abs(color[1] - g_color[i]);	
			diff[i][2] = Math.abs(color[2] - b_color[i]);			
			}
		return min(diff);
	}
	
	public int min(float [][] diff) { //renvoi l'indice de la couleur la plus proche de celle passer en parametre
		int i_r = 0, i_g = 0, i_b = 0;
		float m_r = diff[0][0], m_g = diff[0][1], m_b = diff[0][2];
		for(int i = 1 ; i <nb_color; i++) {
			if(diff[i][0] < m_r) {
				m_r = diff[i][0];
				i_r = i;
			}
			if(diff[i][1] < m_g) {
				m_g = diff[i][1];
				i_g = i;
			}
			if(diff[i][2] < m_b) {
				m_b = diff[i][2];
				i_b = i;
			}		
		}
		//Deduction de la couleur la plus similaire
		if(i_r == i_g)
			return i_r;
		if(i_r == i_b)
			return i_r;
		if(i_g == i_b)
			return i_b;
		else //si on a pas trouver on recommence
			detectionColor();
		return -1;
	}	
}
